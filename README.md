# Simple Bowling Score Calculator

## What Is the Bowling Score Calculator?

> "He who bowls, needs to know his score" - Me (Just now)

This is a program to calculate a bowling score from user provided input.

A game input is a space delimited String.  For example "10 1 2 3 4".

If there is a score of 10 for the first ball of a frame **the frame is finished** and the next score in the list will 
be the start of the next frame.

So from the example above:

* Frame 1: Ball1(10)
* Frame 2: Ball1(2), Ball2(1)
* Frame 3: Ball1(3), Ball2(4) 

If a full game is not entered then the remaining bowls will be scored at 0 and a total gathered.

Validation is performed on the input so if a frame has too many pins, too many scores are entered or another error 
condition is met, then an error will be output rather than a score.

## Requirements

* Java 8
* Maven

## Usage

#### Building the Application

Run the command `mvn package` from the root directory to build and test the solution. 


#### Running the Application

To run the application just launch the jar with dependencies which is found in the target folder.  
Add the game to calculate as a program argument:

`java -jar target/bowlingscorecalculator-1.0.0-jar-with-dependencies.jar "10 10 10 10 10 10 10 10 10 10 10 10"`

Congratulations - you just bowled a perfect game!