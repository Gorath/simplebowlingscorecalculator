package bowlingscorecalculator.model;

import java.util.List;

public class Game {

    private final List<Frame> frameList;
    private final List<BonusBall> bonusBalls;

    public Game(List<Frame> frameList, List<BonusBall> bonusBalls) {
        this.frameList = frameList;
        this.bonusBalls = bonusBalls;
    }

    public List<Frame> getFrameList() {
        return frameList;
    }

    public List<BonusBall> getBonusBalls() {
        return bonusBalls;
    }

    @Override
    public String toString() {
        return String.format("Game{ Frames{ %s }, BonusBalls{ %s }", frameList, bonusBalls);
    }
}
