package bowlingscorecalculator.model;

public class BonusBall {

    private final int bonusBallScore;

    public BonusBall(int bonusBallScore) {
        this.bonusBallScore = bonusBallScore;
    }

    public int getBonusBallScore() {
        return bonusBallScore;
    }

    @Override
    public String toString() {
        return String.format("BonusBall{ %s }", bonusBallScore);
    }
}
