package bowlingscorecalculator.model;

import bowlingscorecalculator.Constants;

public class Frame {

    private final int firstBallScore;
    private final int secondBallScore;

    public Frame(int firstBallScore, int secondBallScore) {
        this.firstBallScore = firstBallScore;
        this.secondBallScore = secondBallScore;
    }

    public int getFirstBallScore() {
        return firstBallScore;
    }

    public int getSecondBallScore() {
        return secondBallScore;
    }

    public boolean isStrike() {
        return firstBallScore == Constants.NUM_PINS;
    }

    public boolean isSpare() {
        return !isStrike() && firstBallScore + secondBallScore == Constants.NUM_PINS;
    }

    @Override
    public String toString() {
        return String.format("Fr{ %s | %s }", firstBallScore, secondBallScore);
    }
}
