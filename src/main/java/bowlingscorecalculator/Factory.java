package bowlingscorecalculator;

import bowlingscorecalculator.model.Game;

public interface Factory {

    /**
     * Parses the input string into a Game object.  It is up to the implementations to define
     * the game structure.
     * @param input
     * @return The game
     * @throws ConstructionException if the game can't be constructed from the input
     */
    Game createGame(String input) throws ConstructionException;

}
