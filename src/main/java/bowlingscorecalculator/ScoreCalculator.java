package bowlingscorecalculator;

import bowlingscorecalculator.model.Game;

public interface ScoreCalculator {

    /**
     * Scores a game.  How a game is scored is left to the implementation.
     * @param theGame the game to score
     * @return an integer representing the score of the game
     */
    int calculateScore(Game theGame);

}
