package bowlingscorecalculator.impl;

import bowlingscorecalculator.Constants;
import bowlingscorecalculator.model.Frame;
import bowlingscorecalculator.ScoreCalculator;
import bowlingscorecalculator.model.Game;

import java.util.List;

/**
 * Requires a Game object which matches the contract set out in FactoryImpl
 * and is validated with ValidationImpl
 */
public class ScoreCalculatorImpl implements ScoreCalculator {

    @Override
    public int calculateScore(Game theGame) {
        final List<Frame> frameList = theGame.getFrameList();
        int total = 0;
        for (int i = 0; i < Constants.NUM_FRAMES; i++) {
            final BallFetcher ballFetcher = createBallFetcher(i, theGame);
            total += calculateFrameScore(frameList.get(i), ballFetcher);
        }
        return total;
    }

    private int calculateFrameScore(Frame frame, BallFetcher ballFetcher) {
        int total = frame.getFirstBallScore() + frame.getSecondBallScore();

        // Add extra balls for strike or spare
        if (frame.isSpare()) {
            total += ballFetcher.getFirstBall();
        } else if (frame.isStrike()) {
            total += ballFetcher.getFirstBall() + ballFetcher.getSecondBall();
        }
        return total;
    }


    private BallFetcher createBallFetcher(int frameNumber, Game theGame) {
        if (frameNumber < Constants.NUM_FRAMES - 2) {
            return new OneToEigthFrameBallFetcher(theGame, frameNumber);
        } else if (frameNumber == Constants.NUM_FRAMES - 2) {
            return new NinthFrameBallFetcher(theGame, frameNumber);
        } else {
            return new TenthFrameBallFetcher(theGame, frameNumber);
        }
    }

    /**
     * These are helper classes to get the scores needed in the event of a strike or spare in a frame
     * They look ahead and fetch the next scores based on the game as defined
     *
     * NB checks here should be safe and not throw exceptions while getting contents of lists
     * These 'naked' fetches will work fine as long as the game has been validated with ValidatorImpl
     *
     * I would normally put these in seperate files but I put them here so it's easier to see who uses what
     */
    abstract class BallFetcher {
        final Game theGame;
        final int frameNumber;

        BallFetcher(Game theGame, int frameNumber) {
            this.theGame = theGame;
            this.frameNumber = frameNumber;
        }

        abstract int getFirstBall();

        abstract int getSecondBall();
    }

    class OneToEigthFrameBallFetcher extends BallFetcher {

        OneToEigthFrameBallFetcher(Game theGame, int frameNumber) {
            super(theGame, frameNumber);
        }

        @Override
        int getFirstBall() {
            return theGame.getFrameList().get(frameNumber + 1).getFirstBallScore();
        }

        @Override
        int getSecondBall() {
            final Frame nextFrame = theGame.getFrameList().get(frameNumber + 1);
            if (nextFrame.isStrike()) {
                return theGame.getFrameList().get(frameNumber + 2).getFirstBallScore();
            }
            return nextFrame.getSecondBallScore();
        }
    }

    class NinthFrameBallFetcher extends BallFetcher {

        NinthFrameBallFetcher(Game theGame, int frameNumber) {
            super(theGame, frameNumber);
        }

        @Override
        int getFirstBall() {
            return theGame.getFrameList().get(frameNumber + 1).getFirstBallScore();
        }

        @Override
        int getSecondBall() {
            final Frame nextFrame = theGame.getFrameList().get(frameNumber + 1);
            if (nextFrame.isStrike()) {
                return theGame.getBonusBalls().get(0).getBonusBallScore();
            }
            return nextFrame.getSecondBallScore();
        }
    }

    class TenthFrameBallFetcher extends BallFetcher {
        TenthFrameBallFetcher(Game theGame, int frameNumber) {
            super(theGame, frameNumber);
        }

        @Override
        int getFirstBall() {
            return theGame.getBonusBalls().get(0).getBonusBallScore();
        }

        @Override
        int getSecondBall() {
            return theGame.getBonusBalls().get(1).getBonusBallScore();
        }
    }

}
