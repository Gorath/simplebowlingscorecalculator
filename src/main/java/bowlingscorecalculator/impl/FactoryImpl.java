package bowlingscorecalculator.impl;

import bowlingscorecalculator.Constants;
import bowlingscorecalculator.ConstructionException;
import bowlingscorecalculator.Factory;
import bowlingscorecalculator.model.BonusBall;
import bowlingscorecalculator.model.Frame;
import bowlingscorecalculator.model.Game;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Creates a game object which has 10 frames
 * If the last frame is a strike there are two bonus balls
 * If the last frame is a spare there is one
 * If the last frame is neither a strike or spare there should be no bonus balls
 */
public class FactoryImpl implements Factory {

    @Override
    public Game createGame(String input) throws ConstructionException {
        Integer[] inputIntegers = convertInputToIntArray(input);
        final Game game = convertIntegerArrayToGame(inputIntegers);
        padEndOfFramesListToTenFrames(game.getFrameList());
        addBonusBallsIfRequired(game);
        return game;
    }

    private void addBonusBallsIfRequired(Game game) {
        int actualNumberOfBonusBalls = game.getBonusBalls().size();
        int expectedNumberOfBonusBalls = 0;
        final Frame tenthFrame = game.getFrameList().get(9);
        if (tenthFrame.isStrike()) {
            expectedNumberOfBonusBalls = 2;
        } else if (tenthFrame.isSpare()) {
            expectedNumberOfBonusBalls = 1;
        }

        while (expectedNumberOfBonusBalls > actualNumberOfBonusBalls) {
            game.getBonusBalls().add(new BonusBall(0));
            actualNumberOfBonusBalls++;
        }
    }

    private void padEndOfFramesListToTenFrames(List<Frame> frames) {
        final int max_frames = Constants.NUM_FRAMES;
        for (int i = frames.size(); i < max_frames; i++) {
            frames.add(new Frame(0,0));
        }
    }

    private Game convertIntegerArrayToGame(Integer[] inputIntegers) throws ConstructionException {
        final List<Frame> frames = new ArrayList<>();
        final List<BonusBall> bonusBalls = new ArrayList<>();
        int frameNumber = 1;
        for (int i = 0; i < inputIntegers.length; i++) {

            if (bonusBalls.size() >= Constants.MAX_NUM_BONUS_BALLS) {
                throw new ConstructionException("Too many balls in game.");
            }

            final GameEntityGenerator entityGenerator = getEntityGenerator(frameNumber);
            i = entityGenerator.generateEntity(inputIntegers, i, frames, bonusBalls);

            frameNumber++;
        }
        return new Game(frames, bonusBalls);
    }

    private Integer[] convertInputToIntArray(String input) throws ConstructionException {
        final String trimmedInput = input.trim();
        checkForEmptyString(trimmedInput);
        try {
            return Arrays.stream(trimmedInput.split(" ")).map(Integer::parseInt).toArray(Integer[]::new);
        } catch (NumberFormatException e) {
            throw new ConstructionException("Error processing input string.  Please ensure only space seperated integers are entered");
        }
    }

    private void checkForEmptyString(String trimmedInput) throws ConstructionException {
        if (trimmedInput.isEmpty()) {
            throw new ConstructionException("Empty input is invalid.  Please ensure input has at least one integer.");
        }
    }

    private GameEntityGenerator getEntityGenerator(int frameNumber) {
        if (frameNumber > Constants.NUM_FRAMES) {
            return new BonusBallGenerator();
        }
        return new FrameGenerator();
    }

    /**
     * These are helper classes to generate the entities in the game (Frames and BonusBalls)
     */
    abstract class GameEntityGenerator {
        abstract int generateEntity(Integer[] scores, int offset, List<Frame> frames, List<BonusBall> bonusBalls);
    }

    class FrameGenerator extends GameEntityGenerator {

        @Override
        int generateEntity(Integer[] scores, int offset, List<Frame> frames, List<BonusBall> bonusBalls) {
            int firstBall = scores[offset];
            int secondBall = 0;

            if (firstBall != Constants.NUM_PINS && offset < scores.length - 1) {
                offset++;
                secondBall = scores[offset];
            }

            frames.add(new Frame(firstBall, secondBall));
            return offset;
        }
    }

    class BonusBallGenerator extends GameEntityGenerator {

        @Override
        int generateEntity(Integer[] scores, int offset, List<Frame> frames, List<BonusBall> bonusBalls) {
            bonusBalls.add(new BonusBall(scores[offset]));
            return offset;
        }
    }
}
