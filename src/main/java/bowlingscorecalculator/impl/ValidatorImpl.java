package bowlingscorecalculator.impl;

import bowlingscorecalculator.Constants;
import bowlingscorecalculator.Validation;
import bowlingscorecalculator.model.BonusBall;
import bowlingscorecalculator.model.Frame;
import bowlingscorecalculator.ValidationException;
import bowlingscorecalculator.model.Game;

import java.util.Arrays;
import java.util.List;

public class ValidatorImpl implements Validation {

    private final List<Validator> frameValidators = Arrays.asList(
            new FirstBallWithinBoundsValidator(),
            new SecondBallInBoundsValidator(),
            new SecondBallZeroOnStrikeValidator(),
            new SumOfTwoBallsDoesntExceedMaxPinsValidator()
    );

    private final CheckBonusBallValuesAreValid bonusBallValidator = new CheckBonusBallValuesAreValid();
    private final CheckBonusBallsInBounds bonusBallsInBounds = new CheckBonusBallsInBounds();

    public void validate(Game game) throws ValidationException {
        for (Frame frame : game.getFrameList()) {
            for (Validator frameValidator : frameValidators) {
                frameValidator.validate(frame);
            }
        }

        // There is guaranteed to be 10 frames if the game was created by FactoryImpl so naked get is ok
        // Keeps the file simpler.  Validation could be added to assert there 10 frames but I wanted to keep
        // the solution simple
        bonusBallValidator.check(game.getFrameList().get(9), game.getBonusBalls());
        bonusBallsInBounds.check(game.getBonusBalls());
    }

    /**
     * Define an interface for validators which can be applied to frames
     *
     * These would usually be in separate files but I didn't want file explosion
     */
    interface Validator {
        void validate(Frame frame) throws ValidationException;
    }

    static class SecondBallZeroOnStrikeValidator implements Validator {
        @Override
        public void validate(Frame frame) throws ValidationException {
            if (frame.isStrike() && frame.getSecondBallScore() != 0) {
                final String error = String.format("Second ball of frame %s not zero when first ball was a strike.", frame);
                throw new ValidationException(error);
            }
        }
    }

    static class SumOfTwoBallsDoesntExceedMaxPinsValidator implements Validator {
        @Override
        public void validate(Frame frame) throws ValidationException {
            final int frameTotal = frame.getFirstBallScore() + frame.getSecondBallScore();
            if (frameTotal < 0 || frameTotal > Constants.NUM_PINS) {
                final String error = String.format("Sum of balls in frame %s needs to be in range 0 <= x <= %s.", frame, Constants.NUM_FRAMES);
                throw new ValidationException(error);
            }
        }
    }

    static class FirstBallWithinBoundsValidator implements Validator {
        @Override
        public void validate(Frame frame) throws ValidationException {
            final int firstBallScore = frame.getFirstBallScore();
            if (firstBallScore < 0 || firstBallScore > Constants.NUM_PINS) {
                final String error = String.format("First ball of frame %s should be in range 0 <= x <= %s.", frame, Constants.NUM_PINS);
                throw new ValidationException(error);
            }
        }
    }

    static class SecondBallInBoundsValidator implements Validator {
        @Override
        public void validate(Frame frame) throws ValidationException {
            final int secondBallScore = frame.getSecondBallScore();
            if (secondBallScore < 0 || secondBallScore > Constants.NUM_PINS) {
                final String error = String.format("Second ball of frame %s should be in range 0 <= x <= %s.", frame, Constants.NUM_PINS);
                throw new ValidationException(error);
            }
        }
    }

    static class CheckBonusBallValuesAreValid {
        public void check(Frame tenthFrame, List<BonusBall> bonusBalls) throws ValidationException {

            if (tenthFrame.isStrike() && bonusBalls.size() != 2) {
                throw new ValidationException("Tenth frame was strike.  Need two bonus balls for score but found " + bonusBalls.size());
            } else if (tenthFrame.isSpare() && bonusBalls.size() != 1) {
                throw new ValidationException("Tenth frame was spare.  Expected one bonus ball but found " + bonusBalls.size());
            } else if (!tenthFrame.isStrike() && !tenthFrame.isSpare() && bonusBalls.size() != 0) {
                throw new ValidationException("Tenth frame was neither a strike or spare.  Expected no bonus balls but found " + bonusBalls.size());
            }
        }

    }

    static class CheckBonusBallsInBounds {
        public void check(List<BonusBall> bonusBalls) throws ValidationException {
            for (BonusBall bonusBall : bonusBalls) {
                final int bonusBallScore = bonusBall.getBonusBallScore();
                if (bonusBallScore < 0 || bonusBallScore > Constants.NUM_PINS) {
                    final String error = String.format("Bonus ball %s value out of range. Should be in range 0 <= x <= %s.", bonusBall, Constants.NUM_PINS);
                    throw new ValidationException(error);
                }
            }
        }
    }

}
