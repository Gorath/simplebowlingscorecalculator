package bowlingscorecalculator;

/**
 * NB I haven't tried changing these constants.  They're just here so there
 * aren't 'magic numbers' littered over the code
 */
public class Constants {

    // Default number of pins for a bowling game
    public static final int NUM_PINS = 10;

    // Default number of frames
    public static final int NUM_FRAMES = 10;

    // Maximum number of bonus balls
    public static final int MAX_NUM_BONUS_BALLS = 2;

}
