package bowlingscorecalculator;

import bowlingscorecalculator.model.Game;

public interface Validation {

    /**
     * Validates a game.  How the validation is performed is left to implementations
     * @param theGame
     * @throws ValidationException
     */
    void validate(Game theGame) throws ValidationException;
}
