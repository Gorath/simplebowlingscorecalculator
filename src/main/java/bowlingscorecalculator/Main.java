package bowlingscorecalculator;

import bowlingscorecalculator.impl.FactoryImpl;
import bowlingscorecalculator.impl.ScoreCalculatorImpl;
import bowlingscorecalculator.impl.ValidatorImpl;
import bowlingscorecalculator.model.Game;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

    private static Logger LOG = LoggerFactory.getLogger(Main.class);

    public static void main(String[] args) {
        // Check arg
        if (args.length != 1) {
            LOG.error("Please enter only one argument which is a space seperated list of integers enclosed in quotes.  E.g. \"1 3 2 5 10\"");
            return;
        }

        // Run the system
        String input = args[0];
        try {
            run(input);
        } catch (ValidationException | ConstructionException e) {
            LOG.error("Caught exception while processing input.  Exception was: " + e.getMessage());
        }
    }

    public static int run(String input) throws ConstructionException, ValidationException {
        LOG.info("Received input '" + input + "'");
        final Game theGame = new FactoryImpl().createGame(input);
        LOG.info("Received input frames.  Constructed game: {}", theGame);
        LOG.debug("About to validate frames.");
        new ValidatorImpl().validate(theGame);
        LOG.debug("Validated.  About to calculate score.");
        final int score = new ScoreCalculatorImpl().calculateScore(theGame);
        LOG.info("Calculated scores.  Total is: {}", score);
        return score;
    }
}
