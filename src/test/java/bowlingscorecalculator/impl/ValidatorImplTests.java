package bowlingscorecalculator.impl;

import bowlingscorecalculator.ValidationException;
import bowlingscorecalculator.model.BonusBall;
import bowlingscorecalculator.model.Frame;
import bowlingscorecalculator.model.Game;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ValidatorImplTests {

    // SecondBallZeroOnStrikeValidator

    @Test(expectedExceptions = ValidationException.class)
    public void testSecondBallZeroOnStrikeValidatorThrows() throws ValidationException {
        final ValidatorImpl.SecondBallZeroOnStrikeValidator validator = new ValidatorImpl.SecondBallZeroOnStrikeValidator();
        validator.validate(new Frame(10, 2));
    }

    @Test
    public void testSecondBallZeroOnStrikeValidatorPassesNoStrike() throws ValidationException {
        final ValidatorImpl.SecondBallZeroOnStrikeValidator validator = new ValidatorImpl.SecondBallZeroOnStrikeValidator();
        validator.validate(new Frame(8, 2));
    }

    @Test
    public void testSecondBallZeroOnStrikeValidatorPassesStrikeZero() throws ValidationException {
        final ValidatorImpl.SecondBallZeroOnStrikeValidator validator = new ValidatorImpl.SecondBallZeroOnStrikeValidator();
        validator.validate(new Frame(10, 0));
    }

    // SumOfTwoBallsDoesntExceedMaxPinsValidator

    @Test(expectedExceptions = ValidationException.class)
    public void testSumOfTwoBallsDoesntExceedMaxPinsValidatorThrows() throws ValidationException {
        final ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator validator = new ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator();
        validator.validate(new Frame(9, 2));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testSumOfTwoBallsDoesntExceedMaxPinsValidatorThrowsUnder() throws ValidationException {
        final ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator validator = new ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator();
        validator.validate(new Frame(-1, 0));
    }

    @Test
    public void testSumOfTwoBallsDoesntExceedMaxPinsValidatorPasses() throws ValidationException {
        final ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator validator = new ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator();
        validator.validate(new Frame(8, 2));
    }

    @Test
    public void testSumOfTwoBallsDoesntExceedMaxPinsValidatorPassesBottomBound() throws ValidationException {
        final ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator validator = new ValidatorImpl.SumOfTwoBallsDoesntExceedMaxPinsValidator();
        validator.validate(new Frame(0,0));
    }


    // FirstBallWithinBoundsValidator
    @Test
    public void testFirstBallWithinBoundsValidatorBottomBound() throws ValidationException {
        final ValidatorImpl.FirstBallWithinBoundsValidator validator = new ValidatorImpl.FirstBallWithinBoundsValidator();
        validator.validate(new Frame(0,0));
    }

    @Test
    public void testFirstBallWithinBoundsValidatorMiddle() throws ValidationException {
        final ValidatorImpl.FirstBallWithinBoundsValidator validator = new ValidatorImpl.FirstBallWithinBoundsValidator();
        validator.validate(new Frame(5,0));
    }

    @Test
    public void testFirstBallWithinBoundsValidatorTopBound() throws ValidationException {
        final ValidatorImpl.FirstBallWithinBoundsValidator validator = new ValidatorImpl.FirstBallWithinBoundsValidator();
        validator.validate(new Frame(10,0));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testFirstBallWithinBoundsValidatorOverTopBound() throws ValidationException {
        final ValidatorImpl.FirstBallWithinBoundsValidator validator = new ValidatorImpl.FirstBallWithinBoundsValidator();
        validator.validate(new Frame(11,0));
    }


    @Test(expectedExceptions = ValidationException.class)
    public void testFirstBallWithinBoundsValidatorUnderBottomBound() throws ValidationException {
        final ValidatorImpl.FirstBallWithinBoundsValidator validator = new ValidatorImpl.FirstBallWithinBoundsValidator();
        validator.validate(new Frame(-1,0));
    }


    // SecondBallInBoundsValidator
    @Test
    public void testSecondBallInBoundsValidatorBottomBound() throws ValidationException {
        final ValidatorImpl.SecondBallInBoundsValidator validator = new ValidatorImpl.SecondBallInBoundsValidator();
        validator.validate(new Frame(0,0));
    }

    @Test
    public void testSecondBallInBoundsValidatorMiddle() throws ValidationException {
        final ValidatorImpl.SecondBallInBoundsValidator validator = new ValidatorImpl.SecondBallInBoundsValidator();
        validator.validate(new Frame(0,5));
    }

    @Test
    public void testSecondBallInBoundsValidatorTopBound() throws ValidationException {
        final ValidatorImpl.SecondBallInBoundsValidator validator = new ValidatorImpl.SecondBallInBoundsValidator();
        validator.validate(new Frame(0,10));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testSecondBallInBoundsValidatorOverTopBound() throws ValidationException {
        final ValidatorImpl.SecondBallInBoundsValidator validator = new ValidatorImpl.SecondBallInBoundsValidator();
        validator.validate(new Frame(0, 11));
    }


    @Test(expectedExceptions = ValidationException.class)
    public void testSecondBallInBoundsValidatorUnderBottomBound() throws ValidationException {
        final ValidatorImpl.SecondBallInBoundsValidator validator = new ValidatorImpl.SecondBallInBoundsValidator();
        validator.validate(new Frame(0, -1));
    }

    // CheckBonusBallValuesAreValid
    @Test(expectedExceptions = ValidationException.class)
    public void testNoBonusBallsTenthFrameStrike() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(10, 0), Collections.emptyList());
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testNoBonusBallsTenthFrameSpare() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(8, 2), Collections.emptyList());
    }

    @Test
    public void testNoBonusBallsTenthFrameNormal() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(3, 4), Collections.emptyList());
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testOneBonusBallsTenthFrameStrike() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(10, 0), Collections.singletonList(new BonusBall(5)));
    }

    @Test
    public void testOneBonusBallsTenthFrameSpare() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(8,2), Collections.singletonList(new BonusBall(5)));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testOneBonusBallsTenthFrameNormal() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(5, 6), Collections.singletonList(new BonusBall(5)));
    }

    @Test
    public void testTwoBonusBallsTenthFrameStrike() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(10, 0), Arrays.asList(new BonusBall(5), new BonusBall(6)));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testTwoBonusBallsTenthFrameSpare() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(8, 2), Arrays.asList(new BonusBall(5), new BonusBall(6)));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testTwoBonusBallsTenthFrameNormal() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(2, 1), Arrays.asList(new BonusBall(5), new BonusBall(6)));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testThreeBonusBallsTenthFrameStrike() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(2, 1), Arrays.asList(
                new BonusBall(5), new BonusBall(6), new BonusBall(7)));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testThreeBonusBallsTenthFrameSpare() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(2, 1), Arrays.asList(
                new BonusBall(5), new BonusBall(6), new BonusBall(7)));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testThreeBonusBallsTenthFrameNormal() throws ValidationException {
        final ValidatorImpl.CheckBonusBallValuesAreValid validator = new ValidatorImpl.CheckBonusBallValuesAreValid();
        validator.check(new Frame(2, 1), Arrays.asList(
                new BonusBall(5), new BonusBall(6), new BonusBall(7)));
    }

    // Test validation passes
    @Test
    public void testValidation() throws ValidationException {
        final ValidatorImpl validator = new ValidatorImpl();
        final List<Frame> frames = Arrays.asList(new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1),
                new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1));
        validator.validate(new Game(frames, Collections.emptyList()));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testBall1Fails() throws ValidationException {
        final ValidatorImpl validator = new ValidatorImpl();
        final List<Frame> frames = Arrays.asList(new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1),
                new Frame(11, -2), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1));
        validator.validate(new Game(frames, Collections.emptyList()));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testBall2Fails() throws ValidationException {
        final ValidatorImpl validator = new ValidatorImpl();
        final List<Frame> frames = Arrays.asList(new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1),
                new Frame(-2, 11), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1));
        validator.validate(new Game(frames, Collections.emptyList()));
    }


    @Test(expectedExceptions = ValidationException.class)
    public void testBallSumFails() throws ValidationException {
        final ValidatorImpl validator = new ValidatorImpl();
        final List<Frame> frames = Arrays.asList(new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1),
                new Frame(1, 11), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1));
        validator.validate(new Game(frames, Collections.emptyList()));
    }

    @Test(expectedExceptions = ValidationException.class)
    public void testStrikeNextBallFilledFails() throws ValidationException {
        final ValidatorImpl validator = new ValidatorImpl();
        final List<Frame> frames = Arrays.asList(new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1),
                new Frame(1, 0), new Frame(0, 1), new Frame(10, 1), new Frame(0, 1), new Frame(0, 1));
        validator.validate(new Game(frames, Collections.emptyList()));
    }


    @Test(expectedExceptions = ValidationException.class)
    public void testNoBonusBallsFails() throws ValidationException {
        final ValidatorImpl validator = new ValidatorImpl();
        final List<Frame> frames = Arrays.asList(new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1),
                new Frame(1, 0), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1), new Frame(0, 1));
        validator.validate(new Game(frames, Collections.singletonList(new BonusBall(5))));
    }


}
