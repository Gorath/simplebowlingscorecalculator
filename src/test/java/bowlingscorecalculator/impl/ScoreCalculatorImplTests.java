package bowlingscorecalculator.impl;

import bowlingscorecalculator.model.BonusBall;
import bowlingscorecalculator.model.Frame;
import bowlingscorecalculator.model.Game;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class ScoreCalculatorImplTests {

    @Test
    public void testFirstFrameNormal() {
        final List<Frame> frames = Arrays.asList(
                createFrame(1, 2),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0)
                );
        final Game game = new Game(frames, Collections.emptyList());
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(3, score);
    }

    @Test
    public void testFirstFrameSpare() {
        final List<Frame> frames = Arrays.asList(
                createFrame(1, 9),
                createFrame(3, 4),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0)
        );
        final Game game = new Game(frames, Collections.emptyList());
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(20, score);
    }

    @Test
    public void testFirstFrameStrike() {
        final List<Frame> frames = Arrays.asList(
                createFrame(10, 0),
                createFrame(3, 4),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0)
        );
        final Game game = new Game(frames, Collections.emptyList());
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(24, score);
    }

    @Test
    public void testFirstFrameStrikeSecondFrameStrike() {
        final List<Frame> frames = Arrays.asList(
                createFrame(10, 0),
                createFrame(10, 0),
                createFrame(5, 3),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0)
        );
        final Game game = new Game(frames, Collections.emptyList());
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(51, score);
    }


    @Test
    public void testNinthFrameNormal() {
        final List<Frame> frames = Arrays.asList(
                createFrame(1, 2),
                createFrame(3, 4),
                createFrame(0, 0),
                createFrame(5, 2),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(2, 7),
                createFrame(2, 2)
        );
        final Game game = new Game(frames, Collections.emptyList());
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(30, score);
    }

    @Test
    public void testNinthFrameSpare() {
        final List<Frame> frames = Arrays.asList(
                createFrame(1, 2),
                createFrame(3, 4),
                createFrame(0, 0),
                createFrame(5, 2),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(3, 7),
                createFrame(2, 2)
        );
        final Game game = new Game(frames, Collections.emptyList());
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(33, score);
    }

    @Test
    public void testNinthFrameStrike() {
        final List<Frame> frames = Arrays.asList(
                createFrame(1, 2),
                createFrame(3, 4),
                createFrame(0, 0),
                createFrame(5, 2),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(10, 0),
                createFrame(2, 2)
        );
        final Game game = new Game(frames, Collections.emptyList());
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(35, score);
    }

    @Test
    public void testNinthFrameStrikeTenthFrameStrike() {
        final List<Frame> frames = Arrays.asList(
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(10, 0),
                createFrame(10, 0)
        );
        final List<BonusBall> bonnusBalls = Arrays.asList(new BonusBall(5), new BonusBall(0));
        final Game game = new Game(frames, bonnusBalls);
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(40, score);
    }

    @Test
    public void testTenthFrameNormal() {
        final List<Frame> frames = Arrays.asList(
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(2, 3)
        );
        final List<BonusBall> bonnusBalls = Arrays.asList(new BonusBall(5), new BonusBall(8));
        final Game game = new Game(frames, bonnusBalls);
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(5, score);
    }

    @Test
    public void testTenthFrameSpare() {
        final List<Frame> frames = Arrays.asList(
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(2, 8)
        );
        final List<BonusBall> bonnusBalls = Arrays.asList(new BonusBall(5), new BonusBall(8));
        final Game game = new Game(frames, bonnusBalls);
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(15, score);
    }

    @Test
    public void testTenthFrameStrike() {
        final List<Frame> frames = Arrays.asList(
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(0, 0),
                createFrame(10, 0)
        );
        final List<BonusBall> bonnusBalls = Arrays.asList(new BonusBall(5), new BonusBall(4));
        final Game game = new Game(frames, bonnusBalls);
        final int score = new ScoreCalculatorImpl().calculateScore(game);
        assertEquals(19, score);
    }

    private Frame createFrame(int firstBallScore, int secondBallScore) {
        return new Frame(firstBallScore, secondBallScore);
    }
}
