package bowlingscorecalculator.impl;

import bowlingscorecalculator.ConstructionException;
import bowlingscorecalculator.model.BonusBall;
import bowlingscorecalculator.model.Frame;
import bowlingscorecalculator.model.Game;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.AssertJUnit.assertEquals;

public class FactoryImplTests {

    @Test(expectedExceptions = ConstructionException.class)
    public void testEmptyInput() throws ConstructionException {
        new FactoryImpl().createGame("    ");
    }

    @Test(expectedExceptions = ConstructionException.class)
    public void testInvalidInput1() throws ConstructionException {
        new FactoryImpl().createGame("1 2 3 a");
    }

    @Test(expectedExceptions = ConstructionException.class)
    public void testInputTooLarge1() throws ConstructionException {
        new FactoryImpl().createGame("1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1 1");
    }

    @Test(expectedExceptions = ConstructionException.class)
    public void testInputTooLarge2() throws ConstructionException {
        new FactoryImpl().createGame("10 10 10 10 10 10 10 10 10 10 10 10 10");
    }

    @Test(expectedExceptions = ConstructionException.class)
    public void testInputTooLarge3() throws ConstructionException {
        new FactoryImpl().createGame("5 5 5 5 5 5 5 5 5 10 10 10 4 2 8 8 10 10 5 4");
    }

    @Test
    public void testPaddingNoBonusBalls() throws ConstructionException {
        final FactoryImpl factory = new FactoryImpl();
        final Game game = factory.createGame("1 2 3 4 5 6 10 10 10 4 2 8 8 8 2 8 1");
        assertEquals(0, game.getBonusBalls().size());
    }

    @Test
    public void testPaddingOneBonusBall() throws ConstructionException {
        final FactoryImpl factory = new FactoryImpl();
        final Game game = factory.createGame("1 2 3 4 5 6 10 10 10 4 2 8 8 8 3 8 2");
        assertEquals(1, game.getBonusBalls().size());
        assertEquals(0, game.getBonusBalls().get(0).getBonusBallScore());
    }

    @Test
    public void testPaddingTwoBonusBalls() throws ConstructionException {
        final FactoryImpl factory = new FactoryImpl();
        final Game game = factory.createGame("1 2 3 4 5 6 10 10 10 4 2 8 8 8 3 10");
        assertEquals(2, game.getBonusBalls().size());
        assertEquals(0, game.getBonusBalls().get(0).getBonusBallScore());
        assertEquals(0, game.getBonusBalls().get(1).getBonusBallScore());
    }

    @Test
    public void testPaddingGameTo10Frames() throws ConstructionException {
        final FactoryImpl factory = new FactoryImpl();
        final Game game = factory.createGame("1 2 3 4 5 6");
        final List<Frame> frameList = game.getFrameList();
        assertEquals(10, frameList.size());
        checkFrameValuesCorrect(frameList.get(0), 1, 2);
        checkFrameValuesCorrect(frameList.get(1), 3, 4);
        checkFrameValuesCorrect(frameList.get(2), 5, 6);
        checkFrameValuesCorrect(frameList.get(3), 0, 0);
        checkFrameValuesCorrect(frameList.get(4), 0, 0);
        checkFrameValuesCorrect(frameList.get(5), 0, 0);
        checkFrameValuesCorrect(frameList.get(6), 0, 0);
        checkFrameValuesCorrect(frameList.get(7), 0, 0);
        checkFrameValuesCorrect(frameList.get(8), 0, 0);
        checkFrameValuesCorrect(frameList.get(9), 0, 0);
    }

    @Test
    public void testPaddingFrame() throws ConstructionException {
        final FactoryImpl factory = new FactoryImpl();
        final Game game = factory.createGame("2 4 6 8 5");
        final List<Frame> frameList = game.getFrameList();
        assertEquals(10, frameList.size());
        checkFrameValuesCorrect(frameList.get(0), 2, 4);
        checkFrameValuesCorrect(frameList.get(1), 6, 8);
        checkFrameValuesCorrect(frameList.get(2), 5, 0);
        checkFrameValuesCorrect(frameList.get(3), 0, 0);
        checkFrameValuesCorrect(frameList.get(4), 0, 0);
        checkFrameValuesCorrect(frameList.get(5), 0, 0);
        checkFrameValuesCorrect(frameList.get(6), 0, 0);
        checkFrameValuesCorrect(frameList.get(7), 0, 0);
        checkFrameValuesCorrect(frameList.get(8), 0, 0);
        checkFrameValuesCorrect(frameList.get(9), 0, 0);
    }

    @Test
    public void testFullWorkingCase() throws ConstructionException {
        final FactoryImpl factory = new FactoryImpl();
        final Game game = factory.createGame("1 2 3 4 5 6 10 10 10 4 2 8 1 8 2 10 4 5");
        final List<Frame> frameList = game.getFrameList();
        assertEquals(10, frameList.size());
        checkFrameValuesCorrect(frameList.get(0), 1, 2);
        checkFrameValuesCorrect(frameList.get(1), 3, 4);
        checkFrameValuesCorrect(frameList.get(2), 5, 6);
        checkFrameValuesCorrect(frameList.get(3), 10, 0);
        checkFrameValuesCorrect(frameList.get(4), 10, 0);
        checkFrameValuesCorrect(frameList.get(5), 10, 0);
        checkFrameValuesCorrect(frameList.get(6), 4, 2);
        checkFrameValuesCorrect(frameList.get(7), 8, 1);
        checkFrameValuesCorrect(frameList.get(8), 8, 2);
        checkFrameValuesCorrect(frameList.get(9), 10, 0);

        final List<BonusBall> bonusBalls = game.getBonusBalls();
        assertEquals(2, bonusBalls.size());
        assertEquals(4, bonusBalls.get(0).getBonusBallScore());
        assertEquals(5, bonusBalls.get(1).getBonusBallScore());
    }

    public void checkFrameValuesCorrect(Frame frame, int firstBall, int secondBall) {
        assertEquals(firstBall, frame.getFirstBallScore());
        assertEquals(secondBall, frame.getSecondBallScore());
    }
}
