package bowlingscorecalculator.integration;

import bowlingscorecalculator.ConstructionException;
import bowlingscorecalculator.Main;
import bowlingscorecalculator.ValidationException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.testng.AssertJUnit.assertEquals;

public class WorkingCasesTests {

    @DataProvider(name = "dataProvider")
    public Object[][] dataProviderNoExcept() throws IOException, URISyntaxException {
        URL url = this.getClass().getClassLoader().getResource("WorkingCases.txt");
        try (Stream<String> stream = Files.lines(Paths.get(url.toURI()))) {
            return stream
                    .filter(s -> !s.startsWith("//")) // Remove comment lines
                    .map(entry -> {
                        String[] split = entry.split("\\|");
                        return new Object[]{split[0], split[1]};
                    })
                    .toArray(Object[][]::new);
        }
    }

    @Test(dataProvider = "dataProvider")
    public void runTest(String input, String output) throws ConstructionException, ValidationException {
        final int expected = Integer.parseInt(output);
        final int actual = Main.run(input);
        assertEquals(expected, actual);
    }
}
