package bowlingscorecalculator.integration;

import bowlingscorecalculator.ConstructionException;
import bowlingscorecalculator.Main;
import bowlingscorecalculator.ValidationException;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.io.IOException;
import java.net.URISyntaxException;

public class ValidationErrorTests extends AbstractErrorTest {

    @DataProvider(name = "errorCasesDataProvider")
    public Object[][] data() throws IOException, URISyntaxException {
        return getData("ValidationErrorCases.txt");
    }

    @Test(dataProvider = "errorCasesDataProvider", expectedExceptions = ValidationException.class)
    protected void runTest(String input) throws ConstructionException, ValidationException {
        Main.run(input);
    }

}
