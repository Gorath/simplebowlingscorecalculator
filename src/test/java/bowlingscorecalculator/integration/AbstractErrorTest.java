package bowlingscorecalculator.integration;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class AbstractErrorTest {
    protected Object[][] getData(String fileName) throws IOException, URISyntaxException {
        URL url = this.getClass().getClassLoader().getResource(fileName);
        try (Stream<String> stream = Files.lines(Paths.get(url.toURI()))) {
            return stream
                    .filter(line -> !line.startsWith("//"))
                    .map(entry -> new Object[]{entry})
                    .toArray(Object[][]::new);
        }
    }
}
